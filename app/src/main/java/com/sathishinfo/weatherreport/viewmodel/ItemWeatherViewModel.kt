package com.sathishinfo.weatherreport.viewmodel

import androidx.databinding.BaseObservable
import com.sathishinfo.weatherreport.model.WeatherModel


class ItemWeatherViewModel(private var weather: WeatherModel) : BaseObservable() {
    val name: String?
        get() = weather.name

    val windSpeed: String?
        get() = weather.wind?.speed?.toString() ?: "0.0"

    val temperatureMin: String?
        get() = weather.main?.tempMin?.toString() ?: "0.0"

    val temperatureMax: String?
        get() = weather.main?.tempMax?.toString() ?: "0.0"

    val description: String?
        get() = weather.weatherList[0].description ?: "No Weather Report"

    fun setWeatherModel(weather: WeatherModel) {
        this.weather = weather
        notifyChange()
    }

}