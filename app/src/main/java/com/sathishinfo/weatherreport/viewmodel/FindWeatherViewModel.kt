package com.sathishinfo.weatherreport.viewmodel

import WeatherFactory
import android.content.Context
import android.text.TextUtils
import com.sathishinfo.weatherreport.MyApplication
import com.sathishinfo.weatherreport.model.WeatherModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class FindWeatherViewModel() : Observable() {
    private val weatherList: MutableList<WeatherModel>
    private var compositeDisposable = CompositeDisposable()

    private fun fetchWeatherList(str: String) {
        val service = MyApplication.create()?.getWeatherService()

        val params: HashMap<String, String> = HashMap()
        params[WeatherFactory.QUERY] = str
        params[WeatherFactory.APPID] = WeatherFactory.APP_ID

        val disposable = service?.fetchWeather(WeatherFactory.FIND_WEATHER_URL, params)
             ?.subscribeOn(Schedulers.newThread())
             ?.observeOn(AndroidSchedulers.mainThread())
             ?.subscribe({ moviesModel ->
                 changeMoviesDataSet(moviesModel)
             }) { throwable ->
                 throwable.printStackTrace()
             }
        disposable?.let { compositeDisposable.add(it) }
    }

    private fun changeMoviesDataSet(weatherModel: WeatherModel?) {
        weatherModel?.let { weatherList.add(it) }
        setChanged()
        notifyObservers()
    }

    fun getWeatherList(): List<WeatherModel> {
        return weatherList
    }

    fun clearWeatherList() {
        weatherList.clear()
        setChanged()
        notifyObservers()
    }

    private fun unSubscribeFromObservable() {
        compositeDisposable.dispose()
    }

    fun reset() {
        unSubscribeFromObservable()
    }

    fun isValidInputString(text: String?, minWords: Int): Boolean {
        if ((text == null || text.isEmpty())) {
            return false
        }
        if (( minWords<=0 )) {
            return false
        }
        val textList = text.split(",")
        if (textList.size < minWords) {
            return false
        }
            for (str in textList) {
                if (str.isEmpty()) {
                    return false
                }
            }

        return true
    }

    fun callWeatherAPI(text: String?, minWords: Int) {
        if (!isValidInputString(text,minWords)) {
            return
        }
        val textList = text?.split(",")
        if (textList != null) {
            for (str in textList) {
                if (str.isNotEmpty()) {
                    fetchWeatherList(str.trim())
                }
            }
        }
    }

    init {
        weatherList = ArrayList()
    }
}