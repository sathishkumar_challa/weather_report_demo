package com.sathishinfo.weatherreport.viewmodel

import androidx.databinding.BaseObservable
import com.sathishinfo.weatherreport.model.ForecastList


class ItemForecastViewModel(private var forecast: ForecastList?) : BaseObservable() {
    val dtTxt: String?
        get() = forecast?.dtTxt ?: ""

    val windSpeed: String?
        get() = forecast?.wind?.speed?.toString() ?: "0.0"

    val temperatureMin: String?
        get() = forecast?.main?.tempMin?.toString() ?: "0.0"

    val temperatureMax: String?
        get() = forecast?.main?.tempMax?.toString() ?: "0.0"

    val description: String?
        get() = forecast?.weather?.get(0)?.description ?: "No Weather Report"

    fun setForecastModel(forecast: ForecastList) {
        this.forecast = forecast
        notifyChange()
    }
}