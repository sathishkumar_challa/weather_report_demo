package com.sathishinfo.weatherreport.viewmodel

import WeatherFactory
import android.content.Context
import com.sathishinfo.weatherreport.MyApplication
import com.sathishinfo.weatherreport.model.ForecastList
import com.sathishinfo.weatherreport.model.ForecastModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import kotlin.collections.HashMap

class ForecastViewModel() : Observable() {
    private var forecastModel: ForecastModel? = null
    private val compositeDisposable = CompositeDisposable()

    val cityName: String?
        get() = forecastModel?.city?.cityName ?: "No Name"

    val countryName: String?
        get() = forecastModel?.city?.country ?: ""


    fun fetchForecastList(lat: Double, lon: Double) {
        val params: HashMap<String, String> = HashMap()
        params[WeatherFactory.LAT] = lat.toString()
        params[WeatherFactory.LON] = lon.toString()
        params[WeatherFactory.APPID] = WeatherFactory.APP_ID

        val service = MyApplication.create()?.getWeatherService()
        val disposable = service?.fetchForecast(WeatherFactory.FORECAST_URL, params)
            ?.subscribeOn(Schedulers.newThread())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({ moviesModel ->
                changeForecastDataSet(moviesModel)
            }) { throwable ->
                throwable.printStackTrace()
            }
        disposable?.let { compositeDisposable.add(it) }
    }

    private fun changeForecastDataSet(forecastModel: ForecastModel?) {

        this.forecastModel = forecastModel
        setChanged()
        notifyObservers()
    }

    fun getForecastList(): List<ForecastList?>? {
        return forecastModel?.list
    }

    private fun unSubscribeFromObservable() {
        compositeDisposable.dispose()
    }

    fun reset() {
        unSubscribeFromObservable()
    }
}