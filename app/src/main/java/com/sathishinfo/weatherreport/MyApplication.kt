package com.sathishinfo.weatherreport

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.sathishinfo.weatherreport.data.IWeatherService

class MyApplication : MultiDexApplication() {

    private var weatherService: IWeatherService? = null



    override fun onCreate() {
        super.onCreate()
        refarence=this
    }



    companion object {
        private var  refarence: MyApplication? = null
        /*private operator fun get(context: Context): MyApplication {
            return context.applicationContext as MyApplication
        }*/
        fun create(): MyApplication? {
            return refarence
        }

    }

    fun getWeatherService(): IWeatherService? {
        if (weatherService == null) {
            weatherService = WeatherFactory.create()
        }
        return weatherService
    }


}