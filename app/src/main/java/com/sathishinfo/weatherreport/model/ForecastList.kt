package com.sathishinfo.weatherreport.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ForecastList {
    @SerializedName("dt")
    @Expose
    var dt: Int? = null
    @SerializedName("main")
    @Expose
    var main: Main? = null
    @SerializedName("weather")
    @Expose
    var weather: List<Weather>? = null
    @SerializedName("wind")
    @Expose
    var wind: Wind? = null
    @SerializedName("dt_txt")
    @Expose
    var dtTxt: String? = null

}