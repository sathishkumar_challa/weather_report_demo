package com.sathishinfo.weatherreport.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class WeatherModel {

    @SerializedName("main")
    @Expose
    var main: Main? = null
    @SerializedName("weather")
    @Expose
    var weatherList: List<Weather> = ArrayList()

    @SerializedName("wind")
    @Expose
    var wind: Wind? = null


    @SerializedName("name")
    @Expose
    var name: String? = null


}