package com.sathishinfo.weatherreport.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class City {
    @SerializedName("name")
    @Expose
    var cityName: String? = null
    @SerializedName("country")
    @Expose
    var country: String? = null

}