package com.sathishinfo.weatherreport.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.sathishinfo.weatherreport.R
import com.sathishinfo.weatherreport.databinding.ItemWeatherBinding
import com.sathishinfo.weatherreport.model.WeatherModel
import com.sathishinfo.weatherreport.view.adapter.WeatherAdapter.WeatherAdapterViewHolder
import com.sathishinfo.weatherreport.viewmodel.ItemWeatherViewModel

class WeatherAdapter : RecyclerView.Adapter<WeatherAdapterViewHolder>() {
    private var weatherList: List<WeatherModel>
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): WeatherAdapterViewHolder {
        val itemBinding = DataBindingUtil.inflate<ItemWeatherBinding>(
            LayoutInflater.from(parent.context), R.layout.item_weather,
            parent, false
        )
        return WeatherAdapterViewHolder(itemBinding)
    }

    override fun onBindViewHolder(
        holder: WeatherAdapterViewHolder,
        position: Int
    ) {
        holder.bindWeather(weatherList[position])
    }

    override fun getItemCount(): Int {
        return weatherList.size
    }

    fun setWeatherList(weatherList: List<WeatherModel>) {
        this.weatherList = weatherList
        notifyDataSetChanged()
    }

    class WeatherAdapterViewHolder(private var binding: ItemWeatherBinding) :
        RecyclerView.ViewHolder(binding.itemMovieRl) {
        fun bindWeather(model: WeatherModel?) {
            if (binding.itemWeatherViewModel == null) {
                val viewmModel = ItemWeatherViewModel(model!!)
                binding.itemWeatherViewModel = viewmModel
            } else {
                val myModel = binding.itemWeatherViewModel as ItemWeatherViewModel
                myModel.setWeatherModel(model!!)
            }
        }

    }

    init {
        weatherList = emptyList()
    }
}