package com.sathishinfo.weatherreport.view

import android.Manifest
import android.annotation.SuppressLint
import android.location.*
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sathishinfo.weatherreport.R
import com.sathishinfo.weatherreport.databinding.CurrentCityWeatherLayoutBinding
import com.sathishinfo.weatherreport.view.adapter.ForecastAdapter
import com.sathishinfo.weatherreport.view.base.AbstractActivity
import com.sathishinfo.weatherreport.viewmodel.ForecastViewModel
import java.util.*


class ForecastActivity : AbstractActivity(), Observer {


    private var forecastBinding: CurrentCityWeatherLayoutBinding? = null
    private var forecastViewModel: ForecastViewModel? = null
    private var currentAddress: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupListMoviesView(forecastBinding?.listForecast)
        setupObserver(forecastViewModel)

    }

    private fun checkLocationPermission() {
        if (isHaveLocationPermission()) {
            getCurrentLocation()
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                LOCATION_REQUEST_CODE
            )
        }
    }

    override fun onResume() {
        super.onResume()
        checkLocationPermission()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (isHaveLocationPermission()) {
            getCurrentLocation()
        } else {
            finish()
        }

    }


    private fun getCurrentAddress(latitude: Double, longitude: Double): String {
        val addresses: List<Address> = Geocoder(this, Locale.getDefault()).getFromLocation(latitude, longitude, 1)
        return addresses[0].getAddressLine(0)
    }


    private fun setupObserver(mainViewMode: ForecastViewModel?) {
        val observer = mainViewMode as Observable
        observer.addObserver(this)

    }

    private fun setupListMoviesView(weatherList: RecyclerView?) {
        weatherList?.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        weatherList?.adapter = ForecastAdapter()
    }

    override fun initDataBinding() {
        forecastBinding = DataBindingUtil.setContentView(this, R.layout.current_city_weather_layout)
        forecastViewModel = ForecastViewModel()
        forecastBinding?.forecastViewModel = forecastViewModel
    }


    override fun update(observable: Observable?, arg: Any?) {

        if (observable is ForecastViewModel) {
            val adapter: ForecastAdapter =
                forecastBinding?.listForecast?.adapter as ForecastAdapter
            val viewModel: ForecastViewModel = observable
            viewModel.getForecastList()?.let { adapter.setForecastList(it) }

            forecastBinding?.cityName?.text = viewModel.cityName
            forecastBinding?.countryName?.text = viewModel.countryName
            forecastBinding?.addressTv?.text = currentAddress
        }


    }

    @SuppressLint("MissingPermission")
    private fun getCurrentLocation() {
        val lm = getSystemService(LOCATION_SERVICE) as LocationManager
        val location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        if (location != null) {
            currentAddress = getCurrentAddress(location.latitude, location.longitude)
            forecastViewModel?.fetchForecastList(location.latitude, location.longitude)
        } else {

            lm.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                2000,
                10.0f,
                object : LocationListener {
                    override fun onLocationChanged(location: Location) {
                        currentAddress = getCurrentAddress(location.latitude, location.longitude)
                        forecastViewModel?.fetchForecastList(
                            location.latitude,
                            location.longitude
                        )
                    }

                    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
                    }

                    override fun onProviderEnabled(provider: String?) {
                    }

                    override fun onProviderDisabled(provider: String?) {
                    }
                })
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        forecastViewModel?.reset()
    }


}


