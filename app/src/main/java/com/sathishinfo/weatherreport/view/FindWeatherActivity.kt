package com.sathishinfo.weatherreport.view

import android.os.Bundle
import android.text.*
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sathishinfo.weatherreport.R
import com.sathishinfo.weatherreport.databinding.FindWeatherLayoutBinding
import com.sathishinfo.weatherreport.view.adapter.WeatherAdapter
import com.sathishinfo.weatherreport.view.base.AbstractActivity
import com.sathishinfo.weatherreport.viewmodel.FindWeatherViewModel
import java.util.*


class FindWeatherActivity : AbstractActivity(), Observer {

    private var findWeatherLayoutBinding: FindWeatherLayoutBinding? = null
    private var findViewModel: FindWeatherViewModel? = null

    private val maxWords: Int by lazy { 7 }
    private val minWords: Int by lazy { 3 }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupListMoviesView(findWeatherLayoutBinding?.listWeather)
        setupObserver(findViewModel)
        myInit()
    }

    private fun myInit() {
        findWeatherLayoutBinding?.searchBtn?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {

                findViewModel?.clearWeatherList()
                val text = findWeatherLayoutBinding?.quiryEt?.text.toString()
                val isValid = findViewModel?.isValidInputString(text, minWords) ?: false
                if (!isValid) {
                    showToastMsz("Please Enter $minWords city names")
                    return
                }

                findViewModel?.callWeatherAPI(text,minWords)

            }
        })
        findWeatherLayoutBinding?.quiryEt?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                findWeatherLayoutBinding?.quiryEt?.removeTextChangedListener(this)
                try {

                    var givenstring = s.toString()
                    val arr = s.toString().trim().split(",")
                    if (arr.size > maxWords) {
                        givenstring = givenstring.substring(
                            0,
                            givenstring.length - 1
                        )
                        findWeatherLayoutBinding?.quiryEt?.setText(givenstring)
                        findWeatherLayoutBinding?.quiryEt?.setSelection(givenstring.length)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                findWeatherLayoutBinding?.quiryEt?.addTextChangedListener(this)
            }
        })

        findWeatherLayoutBinding?.quiryEt?.filters = arrayOf(getInputFilter())
    }


    private fun setupObserver(mainViewMode: FindWeatherViewModel?) {
        val observer = mainViewMode as Observable
        observer.addObserver(this)

    }

    private fun setupListMoviesView(weatherList: RecyclerView?) {
        weatherList?.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        weatherList?.adapter =
            WeatherAdapter()
    }

    override fun initDataBinding() {
        findWeatherLayoutBinding =
            DataBindingUtil.setContentView(this, R.layout.find_weather_layout)
        findViewModel = FindWeatherViewModel()
        findWeatherLayoutBinding?.findViewModel = findViewModel
    }


    override fun update(observable: Observable?, arg: Any?) {

        if (observable is FindWeatherViewModel) {
            val adapter: WeatherAdapter =
                findWeatherLayoutBinding?.listWeather?.adapter as WeatherAdapter
            val viewModel: FindWeatherViewModel = observable
            adapter.setWeatherList(viewModel.getWeatherList())
        }
    }

    private fun getInputFilter(): InputFilter {
        return object : InputFilter {
            override fun filter(
                source: CharSequence,
                start: Int,
                end: Int,
                dest: Spanned,
                dstart: Int,
                dend: Int
            ): CharSequence {
                val sb = StringBuilder(end - start)
                for (i in start until end) {
                    val c = source[i]
                    if (isCharAllowed(c))
                        sb.append(c)
                }
                return sb.toString().replace(",,", ",")
            }

            private fun isCharAllowed(c: Char): Boolean {
                return Character.isLetter(c) || c == ','
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        findViewModel?.reset()
    }

}
