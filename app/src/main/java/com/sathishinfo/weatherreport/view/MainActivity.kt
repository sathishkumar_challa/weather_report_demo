package com.sathishinfo.weatherreport.view

import android.Manifest
import android.content.Intent
import android.view.View
import androidx.core.app.ActivityCompat
import com.sathishinfo.weatherreport.R
import com.sathishinfo.weatherreport.view.base.AbstractActivity


class MainActivity : AbstractActivity() {

    override fun initDataBinding() {
        setContentView(R.layout.activity_main)
    }


    fun onFindWeatherButtonClick(view: View) {
        if (!isNetworkAvailable()) {
            showToastMsz("Please check internet connection")
            return;
        }
        startActivity(Intent(this, FindWeatherActivity::class.java))
    }

    fun onCurrentWeatherButtonClick(view: View) {
        if (isHaveLocationPermission()) {
            if (!isNetworkAvailable()) {
                showToastMsz("Please check internet connection")
                return;
            }
            startActivity(Intent(this, ForecastActivity::class.java))
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ), LOCATION_REQUEST_CODE

            )

        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (isHaveLocationPermission()) {
            startActivity(Intent(this, ForecastActivity::class.java))
        }
    }


}
