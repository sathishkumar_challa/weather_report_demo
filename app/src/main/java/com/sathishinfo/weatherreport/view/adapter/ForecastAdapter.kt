package com.sathishinfo.weatherreport.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.sathishinfo.weatherreport.R
import com.sathishinfo.weatherreport.databinding.ItemForecastBinding
import com.sathishinfo.weatherreport.model.ForecastList
import com.sathishinfo.weatherreport.viewmodel.ItemForecastViewModel

class ForecastAdapter internal constructor() : RecyclerView.Adapter<ForecastAdapter.ForecastAdapterViewHolder>() {
    private var forecastList: List<ForecastList?> = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastAdapterViewHolder {
        val itemBinding = DataBindingUtil.inflate<ItemForecastBinding>(
            LayoutInflater.from(parent.context), R.layout.item_forecast,
            parent, false
        )
        return ForecastAdapterViewHolder(itemBinding)
    }

    override fun onBindViewHolder(
        holder: ForecastAdapterViewHolder,
        position: Int
    ) {
        holder.bindForecast(forecastList[position])
    }

    override fun getItemCount(): Int {
        return forecastList.size
    }

    fun setForecastList(moviesList: List<ForecastList?>) {
        this.forecastList = moviesList
        notifyDataSetChanged()
    }

    class ForecastAdapterViewHolder(private var binding: ItemForecastBinding) :
        RecyclerView.ViewHolder(binding.forecastRelative) {
        fun bindForecast(model: ForecastList?) {
            if (binding.itemForecast == null) {
                val viewModel =
                    ItemForecastViewModel(model)
                binding.itemForecast = viewModel
            } else {
              val myModel=  binding.itemForecast as  ItemForecastViewModel
                myModel.setForecastModel(model!!)
            }
        }

    }

    init {
        forecastList = emptyList()
    }
}