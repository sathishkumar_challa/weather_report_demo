import com.sathishinfo.weatherreport.data.IWeatherService
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Query

object WeatherFactory {

    //api.openweathermap.org/data/2.5/weather?q=London,uk&APPID=f9a62058427f67a191699fcd158058a6
   //https://samples.openweathermap.org/data/2.5/weather?q=india&appid=f9a62058427f67a191699fcd158058a6


    const val APP_ID = "f9a62058427f67a191699fcd158058a6"
    const val BASE_URL = "http://api.openweathermap.org/"

    const val QUERY="q"
    const val LAT="lat"
    const val LON="lon"
    const val APPID="appid"

    //http://api.openweathermap.org/data/2.5/weather?q=London&APPID=f9a62058427f67a191699fcd158058a6
    const val FIND_WEATHER_URL= BASE_URL+"data/2.5/weather";


    //https://samples.openweathermap.org/data/2.5/forecast?q=London&appid=b6907d289e10d714a6e88b30761fae22
    const val FORECAST_URL= BASE_URL+"data/2.5/forecast";


    fun create(): IWeatherService {
        val retrofit = Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
        return retrofit.create(IWeatherService::class.java)
    }
}