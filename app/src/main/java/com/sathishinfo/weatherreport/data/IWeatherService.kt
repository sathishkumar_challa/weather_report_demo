package com.sathishinfo.weatherreport.data

import com.sathishinfo.weatherreport.model.ForecastModel
import com.sathishinfo.weatherreport.model.WeatherModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.QueryMap
import retrofit2.http.Url
import java.util.HashMap

interface IWeatherService {
    @GET
    fun fetchWeather(@Url url: String?,  @QueryMap params: HashMap<String, String> ): io.reactivex.Observable<WeatherModel?>?

    @GET
    fun fetchForecast(@Url url: String?,  @QueryMap params: HashMap<String, String> ): Observable<ForecastModel?>?


}