package com.sathishinfo.weatherreport

import com.sathishinfo.weatherreport.viewmodel.FindWeatherViewModel
import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.runners.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class WeatherValidator {
    private var modelReferance: FindWeatherViewModel? = null

    @Before
    fun setUp() {
        modelReferance = FindWeatherViewModel()
    }

    @Test
    fun weatherValidator_CorrectText_ReturnsTrue() {
        modelReferance?.isValidInputString("oman,ruwi", 2)?.let { assertTrue(it) }
    }

    @Test
    fun weatherValidator_CorrectText_ReturnsFalse() {
        modelReferance?.isValidInputString("oman,ruwi", 3)?.let { assertFalse(it) }
    }

    @Test
    fun weatherValidator_WrongText_ReturnsFalse() {
        modelReferance?.isValidInputString(",,,", 3)?.let { assertFalse(it) }
    }
    @Test
    fun weatherValidator_WrongMinWords_ReturnsFalse() {
        modelReferance?.isValidInputString("oman,ruwi,khammam", -3)?.let { assertFalse(it) }
    }
    @Test
    fun weatherValidator_EmptyText_ReturnsFalse() {
        modelReferance?.isValidInputString("", 3)?.let { assertFalse(it) }
    }

}