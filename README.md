# Weather Report

This is a sample demo application for finding weather report and forecast details. in this API are used from [Open Weather Map](https://openweathermap.org/) provider. 

![](https://firebasestorage.googleapis.com/v0/b/mywebsite-90ac1.appspot.com/o/Screenshot_1.jpg?alt=media&token=9fb15650-0cb1-4798-ba0c-cf4fb8a982d2)

![](https://firebasestorage.googleapis.com/v0/b/mywebsite-90ac1.appspot.com/o/Screenshot_2.jpg?alt=media&token=9fb15650-0cb1-4798-ba0c-cf4fb8a982d2)

![](https://firebasestorage.googleapis.com/v0/b/mywebsite-90ac1.appspot.com/o/Screenshot_3.jpg?alt=media&token=9fb15650-0cb1-4798-ba0c-cf4fb8a982d2)

# The app has following packages:

  - **Data**: It contains all the data accessing and manipulating components.
- **Model**: It contains all the model classes and its having the all business lodic.
- **View**: It contains all the UI related classess and utility related stuff.
- **View Model**: It contains all the view-model classes for interface between model calssess and UI.

 # Data Packege: 
  - **IWeatherService**: This is Interface class contains all webservice methods.
- **WeatherFactory**: This is Factory class for creating instances for WeatherService class .


# Technology used

 - **Clean MVVM architecture**
 -  **Android Databinding**
 -  **RxJava**
 -  **OkHttp**
 -  **Retfofit**





